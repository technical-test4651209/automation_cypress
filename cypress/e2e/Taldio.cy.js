

describe('Project Taldio', () => 
{
    
    context('Register', () => 
    {
        beforeEach(() => 
        {
            cy.visit('https://taldio.com/');
            cy.wait(5000)
            cy.get('#btn_signup').click()
            
        });
    
        it('REGISTER - 001 - Akses Form "Create Account"', () => 
        {
            cy.get('.MuiDialog-container').should('contain', 'Create Account')        
        });

        it('REGISTER - 002 - Klik Button "Sign Up" pada Form Create Account tanpa mengisi required field', () => 
        { 
            cy.get('.gap-2 > #btn_signup').click()
            cy.get('#Personal_FullName-helper-text').should('contain','Full name is required')
            cy.get('#Personal_Email-helper-text').should('contain','Email is required')
            cy.get('#Personal_Handphone-helper-text').should('contain','Phone number contain only numbers')
            cy.get('#Password-helper-text').should('contain','Password is required')
            cy.get('#Re_Password-helper-text').should('contain','Confirm Password is required')
            
        });

        it('REGISTER - 003 - input invalid format email pada field "Email"', () => 
        {
        cy.get('#Personal_Email').type('taldio')
        cy.get('#Personal_Email-helper-text').should('contain','Invalid email format');
        });

        it('REGISTER - 004 - input Phone number less than 9 pada field "Phone Number"', () => 
        {
        cy.get('#Personal_Handphone').type('09')
        cy.get('#Personal_Handphone-helper-text').should('contain','Phone number length must less than 13 and more than 9')
        });

        it('REGISTER - 005 - input Phone number more than 9 pada field "Phone Number"', () => 
        {
            cy.get('#Personal_Handphone').type('01234567891234567899')
            cy.get('#Personal_Handphone-helper-text').should('contain','Phone number length must less than 13 and more than 9')
        });

        it('REGISTER - 006 - input invalid format Password pada field "Password"', () => 
        {
        cy.get('#Password').type('taldio')
        cy.get('#Password-helper-text').should('contain','Password must include at least one uppercase letter and one special character')
        });

        it('REGISTER - 007 - input value yang berbeda pada field "Confirm Password" dengan value yang ada pada field "Password"', () => 
        {
        cy.get('#Password').type('Johndev1@')
        cy.get('#Re_Password').type('taldio')
        cy.get('#Re_Password-helper-text').should('contain','Passwords must match exactly')
        });

        it('REGISTER - 008 - Klik Button "Sign Up" pada Form Create Account dengan mengisi required field dengan benar dan sesuai', () => 
        {
            cy.get('.MuiDialog-container').should('contain', 'Create Account')   
            cy.get('#Personal_FullName').type('Salwa Qurota Ayun')
            cy.get('#Personal_Email').type('ganbatecarmila@gmail.com')
            cy.get('#Personal_Handphone').type('895322424668')
            cy.get('#Password').type('Jhondev1@')
            cy.get('#Re_Password').type('Jhondev1@')
            cy.get('.gap-2 > #btn_signup').click()
            cy.url().should('contain', '/resendemail')
        });   
    });
 
    context('Login', () => 
    {
        beforeEach(() => 
        {
            cy.visit('https://taldio.com/');
            cy.wait(5000)
            cy.get('#btn_signin').click()
            
        });
                        
        it('LOGIN - 001 - Akses Form "Login"', () => 
        {
            cy.get('.MuiDialog-container').should('contain','Sign In')
            
            
        }); 
        
        it('LOGIN - 002 - Klik Button "Sign In" pada Form Create Account tanpa mengisi required field"', () => 
        {
            
            cy.get('#btn_signin_submit').click()
            cy.get('#username-helper-text').should('contain', 'Email is required')
            cy.get('#password-helper-text').should('contain', 'Password is required')
        
        });

        it('LOGIN - 003 - input invalid format email pada field "Email"', () => 
        {
            cy.get('#username').type('Taldio')
            cy.get('#username-helper-text').should('contain', 'Invalid email format')     
        }); 
    
        it('LOGIN - 004 - input email yang tidak terdaftar pada field "Email" dan wrong Password Pada field "Password"', () => 
        {
            cy.get('#username').type('taldio@gmail.com')
            cy.get('#password').type('haha')
            cy.get('#btn_signin_submit').click()
            cy.get('.MuiDialog-container').should('contain','Sign In')  
        }); 

        it('LOGIN - 005 - input email yang terdaftar pada field "Email" dan wrong Password Pada field "Password" ', () => 
        {
            cy.get('#username').type('qurotaayunsalwa@gmail.com')
            cy.get('#password').type('haha')
            cy.get('#btn_signin_submit').click()
            cy.get('.MuiDialog-container').should('contain','Sign In')  
        }); 

        it('LOGIN - 006 - input email yang terdaftar pada field "Email" dan Valid Password Pada field "Password" ', () => 
        {
            cy.get('#username').type('qurotaayunsalwa@gmail.com')
            cy.get('#password').type('Jhondev1@')
            cy.get('#btn_signin_submit').click()
            cy.url().should('contain','/home')
            
        });
                 
    });  
        
    
});





